<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('post_model');
	}

	public function login()
	{
		if($this->session->userdata('logged_in'))
		{
			$this->session->set_flashdata('login_request', 'You are now logged in! You should logout first!');
			redirect(base_url());
		}

		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if($this->form_validation->run() === FALSE){
			$data = array(
				'title' => 'Login',
				'widget' => $this->recaptcha->getWidget(),
				'script' => $this->recaptcha->getScriptTag(),
			);

			$this->load->model('post_model');
			$data['categories'] = $this->post_model->get_categories();
			$this->load->view('templates/header',$data);
			$this->load->view('users/login');
			$this->load->view('templates/footer');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			if(!empty($this->user_model->get_users($username))){
				$enc_password = $this->user_model->get_users($username)->password;
			} else {
				$enc_password = '';
			}
			$this->load->library('bcrypt');

			if ($this->bcrypt->check_password($password, $enc_password)) {
				$user_id = $this->user_model->login($username, $enc_password);
					$user_data = array(
						'user_id' => $user_id,
						'username' => $username,
						'logged_in' => true
					);

					$this->session->set_userdata($user_data);

					$this->session->set_flashdata('user_loggedin', 'You are now logged in');

					redirect(base_url());
				} else {
					$this->session->set_flashdata('login_failed', 'Login is invalid');
					redirect('login');
				}
			}
		}


	public function logout(){
			$this->session->unset_userdata('logged_in');
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('username');

			$this->session->set_flashdata('user_loggedout', 'You are now logged out');

			redirect('login');
		}

	public function register()
	{
		if($this->session->userdata('logged_in'))
		{
			$this->session->set_flashdata('register_request', 'You should logout first!');
			redirect(base_url());
		}



		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('surname', 'surname', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_exists');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('password_again', 'confirm password', 'matches[password]');
		if($this->form_validation->run() === FALSE){
			$recaptcha = $this->input->post('g-recaptcha-response');
			$data = array(
				'title' => 'Register',
				'widget' => $this->recaptcha->getWidget(array('name'=> 'recaptcha')),
				'script' => $this->recaptcha->getScriptTag(),
			);

			$this->load->view('templates/header',$data);
			$this->load->view('users/register');
			$this->load->view('templates/footer');
		} else {
			$this->user_model->register();
			$this->session->set_flashdata('user_loggedin', 'Register Successfully! You can login.');
			redirect('login');
		}

	}

	public function check_username_exists($username){
		$this->form_validation->set_message('check_username_exists', 'That username is taken. Please choose a different one');
		if($this->user_model->check_username_exists($username)){
			return true;
		} else {
			return false;
		}
	}

	public function check_email_exists($email){
		$this->form_validation->set_message('check_email_exists', 'That email is taken. Please choose a different one');
		if($this->user_model->check_email_exists($email)){
			return true;
		} else {
			return false;
		}
	}

	public function edit($id = null){

			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}

			$user_id = $this->session->userdata('user_id');
			$role = $this->user_model->get_users($user_id)->role;
			
			if($role !== 'admin' && $user_id != $this->user_model->get_users($id)->id){
				show_404();
			}

			$data['user'] = $this->user_model->get_users($id);
			if(empty($data['user'])){
				show_404();
			}
			if($role === 'admin'){
				$data['roles'] = $this->user_model->get_roles();
			}

			if($role === 'admin'){
				$data['title'] = 'Edit User';
				$this->load->view('dashboard/users/edit',$data);
			} else {
				$data['title'] = 'Edit User';
				$this->load->view('templates/header',$data);
				$this->load->view('users/edit');
				$this->load->view('templates/footer');
			}
		}

		public function update(){
			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}

			$user_id = $this->session->userdata('user_id');
			$role = $this->user_model->get_users($user_id)->role;
			$id = $this->input->post('id');
			
			if($role !== 'admin' && $user_id != $this->user_model->get_users($id)->id){
				show_404();
			}

			if($role === 'admin'){
				$role_id = (!empty($this->input->post('role_id'))) ? $this->input->post('role_id') : $this->user_model->get_users($user_id)->role_id;
			} else {
				$role_id = $this->user_model->get_users($user_id)->role_id;
				$this->user_model->update_user($id,$role_id);
				$this->session->set_flashdata('user_updated', 'User info has been updated');
				$referred_from = $this->session->userdata('referred_from');
				redirect($referred_from);
			}

			$this->user_model->update_user($id,$role_id);

			$this->session->set_flashdata('user_updated', 'User info has been updated');
		}

		public function profile($username){

			if(!$this->session->userdata('logged_in')){
				redirect('login');
			}
			$data['title'] = 'User Profile';

			$user_id = $this->session->userdata('user_id');
			$data['count_posts'] = count($this->post_model->get_posts_by_author($user_id));
			$user = $this->user_model->get_users($user_id);
			$role = $user->role;
			if($role !== 'admin')
			{
				$data['user'] = $user;
			} else {
				$data['user'] = $this->user_model->get_users($username);
			}

			$this->load->view('templates/header',$data);
			$this->load->view('users/profile');
			$this->load->view('templates/footer');
		}
	
}
