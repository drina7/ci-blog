<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$data['title'] = 'Welcome';
		$this->load->view('templates/header',$data);
		$this->load->view('user/login');
		$this->load->view('templates/footer');
	}
}
