<?php
if($this->session->userdata('logged_in'))
{
  $user_id = $this->session->userdata('user_id');
  $role = $this->user_model->get_users($user_id)->role;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>CI-Blog - <?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= base_url('assets/bootstrap/4.3.1/css/bootstrap.min.css'); ?>">
    <link href="<?= base_url('assets/fontawesome-5.9.0/css/all.min.css');?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/user_profile.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/toastr/toastr3.css'); ?>">
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery/jquery.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/bootstrap/4.3.1/js/bootstrap.min.js'); ?>"></script>

</head>

<body>
    <div class="header">
        <div class="header-content">
            <a href="<?= base_url(); ?>"><img src="<?php echo base_url('assets/images/logo/codeigniter.png'); ?>"></a>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="<?= base_url(); ?>">CI Blog</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?= base_url(); ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active dropdown">
                    <a class="nav-link" href="<?php echo base_url('categories'); ?>">
                        Categories
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <?php if(base_url('login') !== current_url() && base_url('register') !== current_url()) : ?>
                <li>
                    <div class="search">
                        <?php echo form_open(base_url('posts/search'),array('method'=>'get')); ?>
                        <input type="text" name="post" class="form-control input-sm" placeholder="Search Posts">
                        <button type="submit" class="btn btn-secondary btn-sm"><i class="fas fa-search"></i>
                        </button>
                        </form>
                    </div>

                </li>
                <?php endif; ?>

                <?php if(!$this->session->userdata('logged_in')) : ?>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('login') ;?>">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?= base_url('register') ;?>">Register</a>
                </li>
                <?php endif; ?>
                <li class="nav-item dropdown center-block text-white">
                    <?php if($this->session->userdata('logged_in')) : ?>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-circle fa-fw"></i>
                    </a>
                    <div class="dropdown-menu text-dark"
                        style="right: 0; left: auto;min-width: 70px;background-color:rgba(255,255,255,0.5);"
                        aria-labelledby="navbarDropdown">
                        <?php if($role === 'user') : ?>
                        <a class="dropdown-item" href="<?php echo base_url('user/profile'); ?>">Profile</a>
                        <?php endif; ?>
                        <?php if($role !== 'user') : ?>
                        <a class="dropdown-item" href="<?php echo base_url('dashboard'); ?>">Dashboard</a>
                        <?php endif; ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= base_url('user/logout') ;?>">Logout</a>
                        <?php endif; ?>
                </li>
            </ul>
        </div>
    </nav>
    <div style="margin-top: 15px;" class="container">
        <?php if($this->session->flashdata('login_failed')): ?>
        <?php echo '<p class="alert alert-danger">'.$this->session->flashdata('login_failed').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('user_loggedin')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_loggedin').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('user_updated')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('user_updated').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('post_created')): ?>
        <?php echo '<p class="alert alert-success">'.$this->session->flashdata('post_created').'</p>'; ?>
        <?php endif; ?>


        <?php if($this->session->flashdata('user_loggedout')): ?>
        <?php echo '<p class="alert alert-warning">'.$this->session->flashdata('user_loggedout').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('login_request')): ?>
        <?php echo '<p class="alert alert-warning">'.$this->session->flashdata('login_request').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('register_request')): ?>
        <?php echo '<p class="alert alert-warning">'.$this->session->flashdata('register_request').'</p>'; ?>
        <?php endif; ?>

        <?php if($this->session->flashdata('post_deleted')): ?>
        <?php echo '<p class="alert alert-warning">'.$this->session->flashdata('post_deleted').'</p>'; ?>
        <?php endif; ?>