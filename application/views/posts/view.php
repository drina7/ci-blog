<section class="my-5">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-cascade wider reverse">
        <div class="view view-cascade overlay text-center">
          <img class="card-img-top" style="max-height: 500px;width: auto;" src="<?php echo base_url('assets/images/posts/').$post['post_image']; ?>" alt="Sample image">
            <div class="mask rgba-white-slight"></div>
        </div>

        <div class="card-body card-body-cascade text-center">
          <h2 class="font-weight-bold"><a><?php echo $post['title']; ?></a></h2>
          <p>
            <strong>
              <a class="text-dark" href="<?php echo base_url('category/').$post['category_id'];?>">
                <?php echo ucfirst($post_category); ?>
              </a>:
            </strong> Written by <a><strong><?php echo $author; ?></strong></a>, <?php echo $post['created_at']; ?>
          </p>

          <div class="social-counters">
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url(); ?>" target="_blank" class="btn btn-fb">
              <i class="fab fa-facebook-f pr-2"></i>
              <span class="clearfix d-none d-md-inline-block">Facebook</span>
            </a>

            <a href="http://twitter.com/share?text=<?php echo $post['title'];?>&url=<?php echo current_url(); ?>" target="_blank" class="btn btn-tw">
              <i class="fab fa-twitter pr-2"></i>
              <span class="clearfix d-none d-md-inline-block">Twitter</span>
            </a>
            
            <a href="https://plus.google.com/share?url=<?php echo current_url(); ?>" target="_blank"  class="btn btn-gplus">
              <i class="fab fa-google-plus-g pr-2"></i>
              <span class="clearfix d-none d-md-inline-block">Google+</span>
            </a>

          </div>
        </div>
      </div>

      <div class="mt-5">
        <p>
          <?php echo $post['body'];?>
        </p>

        <div class="float-right">
          <?php if( !empty($role) ) : ?>
            <?php if( $this->session->userdata('user_id') == $post['user_id'] || $role === 'admin' ): ?>

              <?php echo form_open('/posts/delete/'.$post['id'],array('id' => 'delete_post')); ?>

              <a class="btn btn-secondary pull-left" href="<?= base_url('dashboard/posts/edit/').$post['id']; ?>">
                Edit
              </a>
              <input type="submit" value="Delete" class="btn btn-danger pull-right">
            </form>

          <?php endif;
                endif; ?>

              </div>
            </div>
          </div>
        </div>
      </section>

      <hr class="mb-5 mt-4">
      <section class="text-center my-5">
        <h2 class="h1-responsive font-weight-bold my-5">Recent posts</h2>
        <div class="row col-md-12">
          <?php

          if( !empty($recents) ) :
            $recents = array_slice($recents, 0,3);
            foreach( $recents as $recent ) :
              $body = strip_tags($recent['body']);
            ?>

            <div class="col-lg-4 col-md-4 mb-lg-0 mb-4" style="height: 510px;">
              <div style="height: 240px" class="view overlay rounded z-depth-2 mb-4">
                <img style="max-height: 240px;width: auto;" class="img-fluid" src="<?php echo base_url('assets/images/posts/').$recent['post_image']; ?>">
              </div>

              <div style="height: 270px" class="view overlay rounded z-depth-2 mb-4">
                <a href="<?php echo base_url('category/').$post['category_id'];?>" class="text-dark">

                  <h6 class="font-weight-bold mb-3">
                    <i class="fas fa-list text-primary"></i> <?php echo ucfirst($recent['name']); ?>
                  </h6>
                </a>

                <h4 class="font-weight-bold mb-3">
                  <strong>
                    <a class="text-dark" href="<?php echo base_url('/posts/'.$recent['post_slug'].'/'.$recent['post_id']); ?>">
                      <?php echo character_limiter($recent['title'],30); ?>
                      </a>
                    </strong>
                  </h4>
                  <p>by <a class="font-weight-bold"><?php echo $recent['author']; ?></a>, 15/07/2018</p>

                  <div style="height: 70px;" class="container">

                    <p class="dark-grey-text"><?php echo character_limiter($body,60); ?></p>
                  </div>
                  <a class="btn btn-secondary btn-rounded btn-md text-white" href="<?php echo base_url('/posts/'.$recent['slug'].'/'.$recent['post_id']); ?>">Read more</a>
                </div>
              </div>
              <?php endforeach;
                    endif; ?>
                  </div>
                </section>

                <div class="card mb-3 wow fadeIn">
                  <div class="card-header font-weight-bold">Leave a Comment</div>
                  <div class="card-body">
                    <div id="message" style="height: 55px"></div>
                    <div class="form-group">
                      <form method="POST" id="myform">
                        <label for="comment">Your comment</label>
                        <textarea class="form-control required" name="body" id="body" rows="5"></textarea>
                      </div>
                      <div class="mt-4">
                        <button id="comment" class="btn btn-info btn-md" type="button">Submit</button>
                      </form>
                    </div>
                  </div>
                </div>


                <div class="card mb-3 wow">
                  <div class="card-header font-weight-bold">
                    <i class="far fa-comments pr-2"></i> Comments:
                  </div>
                  <div class="card-body">
                    <br>
                    <div class="container" id="comments">
                    </div>
                    <div id="nomore"></div>
                    <button id="loadmore" class="btn btn-info btn-md" type="button">
                      Load More
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </section>

<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-validation/jquery.validate.js'); ?>"></script>

<script type="text/javascript">
  $(document).ready(function() {
    var post_id = "<?php echo $post['id']; ?>";
    var logged_user_id = "<?php echo $this->session->userdata('user_id'); ?>";
    var role = "<?php echo $role; ?>";
    var page = 1;
    var per_page = 4;
    var total = <?php echo $comments_count; ?>;
    var limit  = Math.ceil(total/per_page);
    showcomments(page);

    if(total <= per_page) {
      $('#loadmore').remove();
      $('#nomore').replaceWith('<p id="none">No Comments</p><br>');
    }


    $("#loadmore").click(function() {
      if(page <= limit) {
        page++;
        showcomments(page);
      }
    });


    function showcomments(page){

      if(!$.isNumeric(page)){
        page=1;
      }

      $.ajax({
        url: '<?php echo base_url('api/api/get_comments/'); ?>'+post_id+'/?page='+page,
        type: 'GET',
        dataType: 'JSON',
        success: function(response) {
          var len = response.length;
          if(len == 0) {
            $("#nomore").append('<p>No more comments</p><br>');
            $("#loadmore").remove();
          }
          for(var i=0; i<len; i++) {
            $("#none").remove();
            var comment_id = response[i].comment_id;
            var username = response[i].username;
            var user_id = response[i].user_id;
            var body = response[i].body;
            var date = response[i].created_at;

            var comments = '<div id="insert_comment"></div>'+
            '<div id="comment'+comment_id+'" style="margin-bottom:15px;" class="media">'+
            '<div class="media-body">'+
            '<i class="fas fa-user-circle fa-fw"></i>'+
            '<strong>'+username+'</strong>'+
            '<div class="container" style="font-size:8px"><em></em>'+date+'</div>'+
               body+
            '<div id="delete'+comment_id+'" style="margin-top: 5px;"></div>'+
            '</div>'+
            '</div>';

            var del = '<form>'+
            '<button class="btn btn-danger pull-right" id="delete_comment'+comment_id+'" data-id="'+comment_id+'" type="button">Delete</button>'+
            '</form>';

            $("#comments").append(comments);
            if( logged_user_id == user_id || role === 'admin' ) {
              $("#delete"+comment_id).append(del);
            }

            $('#delete_comment'+comment_id).click(function() {
              if (confirm("Are you sure?")) {
                var comment = $(this).attr("data-id");
                $.ajax({
                  type : 'post',
                  url : '<?php echo base_url("api/authapi/delete_comment/"); ?>',
                  data: {id:comment},
                  dataSrc: '',
                  success : function(data){
                    $('#comment'+comment).remove();
                  },
                  error: function(data){
                    toastr.error(
                      'Something went wrong!','Error',
                      {
                        timeOut: 1000,
                        fadeOut: 500
                      });
                  }
                });
              }
              return false;
            });
          }
        }
      });
    }
 $('#confirm-delete').on('show.bs.modal', function(e) {
            $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });

    $('#delete_post').submit(function() {
      var del = confirm("Are you sure?");
      return del;
    });

    $('#myform').validate();
    $('#comment').click(function () {
      $("#success").remove();
      $("#error").remove();
      if ($("#myform").valid()) {
        var comment = $('textarea#body').val();

        $.ajax({
          type : 'post',
          url : "<?php echo base_url('api/authapi/create_comment/'); ?>"+post_id,
          data: {comment:comment},
          dataSrc: '',
          success : function(data){
            $("#success").remove();
            $("textarea#body").val('');
            $('#message').append( '<div id="success" class="alert alert-success">Your comment added successfully!</div>' );
            $("#comments").empty();
            return showcomments();
          },
          error: function(data){
            $("#error").remove();
            $('#message').append( '<div id="error" class="alert alert-danger">You should login first!</div>' );
          }
        });
      }
    });


  });
</script>
