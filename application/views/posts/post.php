<?php 
if(isset($posts)) : 
foreach($posts as $post) :
$body = strip_tags($post['body']);
$username = $post['username'];
?>
<div class="row">


    <div class="col-lg-5 col-xl-4">


        <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4">
            <img class="img-fluid" src="<?php echo base_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>"
                alt="<?php echo $post['title']; ?>">
            <a>
                <div class="mask rgba-white-slight"></div>
            </a>
        </div>

    </div>


    <div class="col-lg-7 col-xl-8">

        <h3 class="font-weight-bold mb-3">
            <strong>
                <a href="<?php echo base_url('/posts/'.$post['post_slug'].'/'.$post['post_id']); ?>">
                    <?php echo $post['title']; ?>
                </a>
            </strong>
        </h3>
        <p class="dark-grey-text"><?php echo word_limiter($body, 60); ?></p>

        <p>by <a href="<?php echo base_url('posts/author/'.$username); ?>" class="font-weight-bold">

                <?php echo $post['author']; ?>

            </a>,
            <em style="font-size:12px;"><?php echo $post['created_at']; ?></em>
        </p>

        <a class="btn btn-primary btn-md"
            href="<?php echo base_url('/posts/'.$post['post_slug'].'/'.$post['post_id']); ?>">Read more</a>

    </div>


</div>


<hr class="my-5">

<?php endforeach;
      endif; ?>