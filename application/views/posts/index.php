<section class="my-5">

    <!-- Section heading -->
    <h2 class="h1-responsive font-weight-bold text-center my-5"><?= $title ?></h2>
    <!-- Section description -->
    <p class="text-center dark-grey-text w-responsive mx-auto mb-5">Duis aute irure dolor in reprehenderit in
        voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    <div class="row">
        <h2><strong><?php if(isset($nothing)){echo $nothing;}?></strong></h2>
    </div>
    <div class="col-md-12" id="post-data">

        <?php

      $this->load->view('posts/post', $posts);

    ?>

    </div>

</section>


<div class="ajax-load text-center" style="display:none">

    <p><img src="<?php echo base_url('assets/images/loader.gif'); ?>">Loading More Posts</p>

</div>

<div class="container text-center" id="end"></div>


<script type="text/javascript">
var limit = <?= $limit; ?> ;
var page = 1;

$(window).scroll(function() {

    if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.5) {

        page++;

        loadMoreData(page);

    }

});

function loadMoreData(page) {
    if (limit >= page) {
        $.ajax({

            url: "<?= base_url('api/api/get_posts/'); ?>" + page,
            type: "get",
            beforeSend: function() {
                $('.ajax-load').show();
            },
            success: function(response) {
                $('.ajax-load').hide();
                $("#post-data").append(response);
            },

            error: function(response) {
                $("#end").append('No posts!');
            }

        });

        if (limit == page) {
            $("#end").append('No more posts!');
        }
    }


}
</script>