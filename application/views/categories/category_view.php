<?php foreach($categories as $category) :
	$posts = $this->category_model->get_posts($category['id']);
	$count = count($posts);
	foreach($posts as $post)
	{
		$image = $post['post_image'];
	}

	if($count == 0)
	{
		$image = 'noimage.png';
	}

	?>

<div class="col-sm-4 py-2">
    <div class="card h-100 border-primary">
        <div style="min-height: 310px; max-height: : 311px" class="card-body">
            <div style="min-height: 200px;max-height: : 200px" class="text-center">
                <img style="max-height: 300px;max-width: 250px;"
                    src="<?php echo base_url('assets/images/posts/'.$image); ?>">
            </div>
            <div class="mt-3">
                <h3 class="card-title text-center">
                    <a href="<?php echo base_url('category/').$category['id']; ?>">
                        <?php echo ucfirst($category['name']); ?>
                    </a>
                </h3>
            </div>
            <div>
                <p class="float-left"> Total Posts: <?php echo $count;?> posts </p>
                <a href="<?php echo base_url('category/').$category['id']; ?>"
                    class="btn btn-outline-primary float-right">

                    View

                </a>
            </div>
        </div>
    </div>
</div>

<?php endforeach; ?>