<div class="container">
    <h5>Categories</h5>
    <div class="row" id="post-data">
        <?php $this->load->view('categories/category_view', $categories); ?>
    </div>

</div>



<div class="ajax-load text-center" style="display:none">

    <p><img src="<?php echo base_url('assets/images/loader.gif'); ?>">Loading More Categories</p>

</div>

<div class="container text-center" id="end"></div>

<script type="text/javascript">
var limit = <?= $limit; ?>;
var page = 1;

$(window).scroll(function() {

    if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.5) {

        page++;

        loadMoreData(page);

    }

});

function loadMoreData(page) {
    if (limit >= page) {
        $.ajax({

            url: "<?php echo base_url('api/api/get_categories/'); ?>" + page,
            type: "get",
            beforeSend: function() {
                $('.ajax-load').show();
            },
            success: function(response) {
                $('.ajax-load').hide();
                $("#post-data").append(response);
            },

            error: function(response) {
                $("#end").append('No categories!');
            }

        });

        if (limit == page) {
            $("#end").append('No more categories!<br>');
        }
    }


}
</script>