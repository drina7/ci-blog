<style type="text/css">
tr {
    border-bottom: 1px solid #c8ced3;
}
</style>
<div class="col-md-12 justify-content-md-center">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-3 border-right">
                    <h4>Add New Category</h4>
                </div>


                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content modal-lg">
                            <div class="modal-header text-center">
                                <h2>Create Category</h2>
                                <button type="submit" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <?php form_open(''); ?>
                                <input type="text" name="category" id="category">
                                <button type="button" class="btn btn-info text-white float-right"
                                    id="submit">Save</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-3">
                    <a data-toggle="modal" data-target="#myModal"><button class="btn btn-success">New</button></a>
                </div>
            </div>
            <hr>
            <div class="row" style="text-align: center;">
                <div class="col-md-8">

                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th colspan="2" style="width: 30%;">Actions</th>
                            </tr>

                        </thead>
                        <tbody id="form-list-client-body">
                            <?php
foreach ($categories as $category) : ?>
                            <tr>
                                <td><?php echo $category['name']; ?></td>
                                <td style="border: none;">
                                    <a href="#" class="text-info"><i style="font-size:19px;"
                                            class="fas fa-pen-square"></i></a>
                                </td>
                                <td style="border: none;">
                                    <a href="#" class="text-danger"><i style="font-size:16px;"
                                            class="fas fa-trash-alt "></i></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>


                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    $('#myModal').on('show.bs.modal');

    $('#submit').click(function() {
        var category = $('#category').val();
        alert(category);
        $.ajax({
            type: 'post',
            url: '<?php echo base_url(); ?>category/create',
            data: {
                category: category
            },
            dataSrc: '',
            success: function(data) {
                toastr.success(
                    'Category added successfully!', 'Success', {
                        timeOut: 1000,
                        fadeOut: 500,
                        onHidden: function() {
                            window.location.reload();
                        }
                    }
                );
            },
            error: function(data) {
                alert(JSON.stringify(data));
            }
        });
    });



});
</script>