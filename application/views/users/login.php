<div class="row">
    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
            <div class="card-body">
                <h5 class="card-title text-center">Sign In</h5>
                <?php
        $attributes = array('class' => 'form-signin', 'id' => 'myform');
        echo form_open('login',$attributes);
        ?>
                <div class="form-label-group">
                    <input type="text" id="username" name="username" class="form-control"
                        placeholder="Username or Email" required autofocus>
                    <label for="username">Username</label>
                </div>

                <div class="form-label-group">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password"
                        required>
                    <label for="password">Password</label>
                </div>

                <div class="custom-control custom-checkbox mb-3">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Remember password</label>
                </div>

                <div style="text-align: center;">
                    <button class="btn btn-lg btn-primary btn-block text-uppercase" id="submit" type="submit"
                        name="submit">Sign in</button>
                    <small class="signup"><strong>New Here? <a href="<?= base_url('register'); ?>">Sign
                                Up</a></strong></small>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function() {

});
</script>