<div class="row">
    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
            <div class="card-body">

                <?php 
        $attributes = array('class' => 'form-signin', 'id' => 'myform');
        echo form_open('register',$attributes);
        ?>


                <hr class="my-4">
                <h5 class="card-title text-center">Sign Up</h5>


                <div id="inline-block">
                    <div class="form-label-group input-name">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Name"
                            value="<?php echo $this->input->post('name'); ?>" autofocus>
                        <label for="name">Name</label>
                        <?= form_error('name', '<div class="error">', '</div>'); ?>
                    </div>

                    <div class="form-label-group input-surname">
                        <input type="text" id="surname" name="surname" class="form-control" placeholder="Surname"
                            value="<?php echo $this->input->post('surname'); ?>" autofocus>
                        <label for="surname">Surname</label>
                        <?= form_error('surname', '<div class="error">', '</div>'); ?>
                    </div>
                </div>
                <div class="form-label-group">
                    <input type="text" id="username" name="username" class="form-control" placeholder="Username"
                        value="<?php echo $this->input->post('username'); ?>" autofocus>
                    <label for="username">Username</label>
                    <?= form_error('username', '<div class="error">', '</div>'); ?>
                </div>

                <div class="form-label-group">
                    <input type="text" id="email" name="email" class="form-control" placeholder="Email Adress"
                        value="<?php echo $this->input->post('email'); ?>" autofocus>
                    <label for="email">Email</label>
                    <?= form_error('email', '<div class="error">', '</div>'); ?>
                </div>

                <div class="form-label-group">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                    <label for="password">Password</label>
                    <?= form_error('password', '<div class="error">', '</div>'); ?>
                </div>

                <div class="form-label-group">
                    <input type="password" id="password_again" name="password_again" class="form-control"
                        placeholder="Repeat Password">
                    <label for="password_again">Repeat Password</label>
                    <?= form_error('password_again', '<div class="error">', '</div>'); ?>
                </div>

                <br>
                <div id="align-center">
                    <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign Up</button>
                    <small class="signup"><strong>Already register? <a href="<?= base_url('login'); ?>">Sign
                                In</a></strong></small>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/js/jquery-validation/jquery.validate.js'); ?>"></script>
<script type="text/javascript">
$(function() {
    jQuery.validator.addMethod("noSpace", function(value, element) { //Code used for blank space Validation 
        return value.indexOf(" ") < 0;
    }, "Username not allowed space!");
    $("#myform").validate({
        errorElement: 'div',
        errorClass: "text-danger font-weight-normal",
        validClass: "text-success",
        rules: {
            name: "required",
            surname: "required",
            username: {
                noSpace: true,
                required: true,
                minlength: 4
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            password_again: {
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            name: "Name is required",
            surname: "Surname is required",
            username: {
                required: "Username is required",
                minlength: "Your username must be at least 5 characters long"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            password_again: {
                equalTo: "Please enter the same password again"
            },
            email: {
                required: "Email is required",
                minlength: "Please enter a valid email address"
            }

        },
        submitHandler: function(form) {
          form.prevent();
          alert('ddddd');
        }
    });
});
</script>