<?php
	class Category_model extends CI_Model{
		public function __construct(){
			$this->load->database();
			$this->load->model('post_model');
		}

		public function create_category(){
			$slug = preg_replace('!\s+!', '-', $this->input->post('category'));
			$data = array(
				'name' => $this->input->post('category'),
				'slug' => $slug
			);

			return $this->db->insert('categories', $data);
		}

		public function get_categories($limit = null, $offset = null){
			if($limit)
			{
				$this->db->limit($limit, $offset);
			}

			$categories = $this->db->get('categories')->result_array();
			if(!empty($categories))
			{
				foreach($categories as $key => &$val){
					$id = $categories[$key]['id'];
					$val['post_count'] = $this->get_posts_count($id)->post_count;
				}
				return $categories;
			}
		}

		public function get_posts_count($category_id){
			$this->db->select('*,count(category_id) as post_count,categories.name as category_name')
			->from('categories')
			->join('posts', 'category_id = categories.id')
			->where('category_id',$category_id);
			$query = $this->db->get();
			return $query->row();
		}

		public function get_posts($category_id){
			$posts = $this->post_model->get_posts_by_category($category_id);
			return $posts;
		}

		public function update_category($category_id){
			$this->db->where('id',$category_id);
			return $this->db->update('categories');
		}

		public function delete_category($category_id){
			$this->db->where('id', $category_id);
			$this->db->delete('categories');
			return true;
		}

		public function check_category_exists(){
			$query = $this->db->get_where('categories', array('name' => $this->input->post('category') ));
			if(empty($query->row_array())){
				return true;
			} else {
				return false;
			}
		}
	}
