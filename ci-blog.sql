-- phpMyAdmin SQL Dump
-- version 4.4.15.9
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2019 at 12:21 PM
-- Server version: 5.6.37
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci-blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`) VALUES
(1, 'technology', 'technology'),
(2, 'sports', 'sports'),
(3, 'gossip', 'gossip'),
(10, 'football', 'football'),
(11, 'tenis', 'tenis'),
(13, 'ddddddddd', 'ddddddddd'),
(16, 'ggggggg', 'ggggggg'),
(36, 'tenisv', 'tenisv'),
(37, 'sportsmfmffkf', 'sportsmfmffkf'),
(38, 'fjfjjjjjjjjjjjjj', 'fjfjjjjjjjjjjjjj'),
(40, 'sssssssssssssss', 'sssssssssssssss'),
(41, 'vvjj', 'vvjj'),
(42, 'kkkk', 'kkkk'),
(43, 'q', 'q'),
(44, 'm', 'm'),
(45, 'fbnf', 'fbnf');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL,
  `body` varchar(1000) NOT NULL,
  `hide` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `post_image` varchar(255) NOT NULL,
  `notification_view` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trash` int(11) NOT NULL DEFAULT '0',
  `views` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `body`, `post_image`, `notification_view`, `created_at`, `trash`, `views`, `category_id`, `user_id`) VALUES
(37, 'Election 2019: What big tech isn''t telling us about ads', 'Election-2019-What-big-tech-isnt-telling-us-about', '<p>All over the country, voters, reporters and other observers have been going through something of an awakening about the extent that political parties are using social media to target us.</p>\r\n\r\n<p><a href="https://www.bbc.co.uk/news/world-us-canada-48972327">In a post-Cambridge Analytica-scandal world</a> there&#39;s suspicion about how we might be being manipulated.</p>\r\n\r\n<p>To address concerns, the tech giants have created databases to show what political adverts are being run and by whom.</p>\r\n\r\n<p>There&#39;s no doubt that more is being disclosed than in previous campaigns, but critics say there&#39;s still much more that &quot;big tech&quot; could reveal.</p>\r\n\r\n<p>Let&#39;s start with Facebook.</p>\r\n\r\n<p>In October last year, it launched its Ad Library in the UK. Since then, more than 131,400 adverts related to politics, elections, and social issues have been added to the database. That&#39;s almost &pound;11m worth, according to the firm.</p>\r\n\r\n<p>The library is free to use and easy to navigate. And recently it began including ads run on the firm&#39;s photo-centric app, Instagram.</p>\r\n\r\n<p>You can look up roughly how many times an ad was seen, its approximate cost, the gender and age of those targeted and who made and paid for the advert.</p>\r\n\r\n<p>You can also see whether an ad was aimed at people in England, Scotland, Wales and/or Northern Ireland.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2135922024.png', 0, '2019-11-16 11:48:38', 0, 1, 1, 9),
(38, 'Tech Tent: Talking to Mr Raspberry Pi', 'Tech-Tent-Talking-to-Mr-Raspberry-Pi', '<p><em><strong>It was a scheme with limited ambitions - getting more young people with coding skills to apply for a university course.</strong></em></p>\r\n\r\n<p><em>But seven years after its launch, Raspberry Pi has become one of the most successful computers in history. On this week&#39;s Tech Tent, we talk to the project&#39;s founder about where it came from and what is next.</em></p>\r\n\r\n<h2><strong>Small start</strong></h2>\r\n\r\n<p>This week, Pi creator Eben Upton was honoured with the Lovie Award for lifetime achievement, recognition of the extraordinary impact that the Raspberry Pi has had.</p>\r\n\r\n<p>It was conceived as a charitable venture at a time when Eben Upton was interviewing applicants for the computer science course at Cambridge University and was disappointed at the number and calibre of the young people he was seeing.</p>\r\n\r\n<p>When I met him this week at Raspberry Pi&#39;s commercial headquarters in Cambridge - which is churning out substantial amounts of revenue for the charitable arm - he told me that as far as the original aim was concerned, it was job done.</p>\r\n\r\n<p>From a low of 200 applicants to study computing, they were now up to 1,100.</p>\r\n\r\n<p>&quot;We have twice as many people applying to study computer science at Cambridge as we had at the height of the dotcom boom. And I understand from people that are still involved in the admissions process, that when you ask them how did you get into computers, they do say &#39;Raspberry Pi.&#39;&quot;</p>\r\n\r\n<p>But what was unexpected - and drove the project off course for a while - was the huge enthusiasm not from children but from 40-something hobbyists who saw in the Raspberry Pi the rebirth of computers like the BBC Micro which they had cut their teeth on in the 1980s.</p>\r\n', '876424200.jpg', 0, '2019-11-16 12:04:27', 0, 1, 1, 9),
(39, 'Will fibre broadband be obsolete by 2030 - and what about 5G?', 'Will-fibre-broadband-be-obsolete-by-2030-and-what', '<p><strong>Labour has promised to give every home and business in the UK free full-fibre broadband by 2030 if it wins the general election.</strong></p>\r\n\r\n<p>The plan would see millions more properties given access to a full-fibre connection, though Prime Minister Boris Johnson said it was &quot;a crackpot scheme&quot;.</p>\r\n\r\n<p>If the plan went ahead and was completed on time, would it still be useful in 2030?</p>\r\n\r\n<h2>What is full-fibre broadband?</h2>\r\n\r\n<p>There are three main types of broadband connection that link the local telephone exchange to your home or office:</p>\r\n\r\n<ul>\r\n	<li>ADSL (asymmetric digital subscriber line) uses copper cables to a street-level cabinet or junction box and on to the house</li>\r\n	<li>FTTC (fibre to the cabinet) uses a faster fibre optic cable to the cabinet, but then copper cable from there to the house</li>\r\n	<li>FTTP (fibre to the premises) uses a fibre optic cable to connect to households without using any copper cable</li>\r\n</ul>\r\n\r\n<h2><strong>How fast is full-fibre?</strong></h2>\r\n\r\n<p>Currently, the UK government defines <strong>superfast</strong> broadband as having speeds greater than 30 megabits per second (Mbps). Megabits per second is the standard measurement of internet speed.</p>\r\n\r\n<p><strong>Ultrafast </strong>is defined as a speed greater than 100Mbps.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '795343015.jpg', 0, '2019-11-16 12:13:45', 0, 1, 1, 9),
(40, 'Uber''s paradox: Gig work app traps and frees its drivers', 'Ubers-paradox-Gig-work-app-traps-and-frees-its-drivers', '<p>On 24 November, after a nervous wait, Uber will learn whether its licence to operate in London is to be renewed.</p>\r\n\r\n<p>The impending decision has revived debate over whether the data-driven basis for its business model and the &quot;gig economy&quot; jobs it creates are fair.</p>\r\n\r\n<p>A wave of platforms has followed, offering new ways to buy and sell, to rent from and temporarily hire others.</p>\r\n\r\n<p>Rather than salaried employees, independent contractors are paid by consumers for a specific job - a &quot;gig&quot;.</p>\r\n\r\n<p>The platforms in the middle argue they do not employ staff but simply connect customers with people seeking to make money.</p>\r\n\r\n<p>Research by the Trades Union Congress (TUC) estimates that one in 10 workers in the UK now regularly does &quot;platform work&quot;.</p>\r\n\r\n<p>No company is more symbolic of this shift than Uber itself.</p>\r\n\r\n<p>As a consequence, it has become a lightning rod for arguments about what gig work really represents.</p>\r\n\r\n<p>Does it usher in new, flexible, liberating ways to work, or is it the means for a kind of arms-length control that undermines basic rights?</p>\r\n\r\n<p>Abdura Hadi, an Uber driver who has worked on the streets of London for five years, has noticed a change.</p>\r\n', '1140944663.jpg', 0, '2019-11-16 12:16:37', 0, 1, 1, 9),
(41, 'Amazon begins appeal over Pentagon cloud contract', 'Amazon-begins-appeal-over-Pentagon-cloud-contract', '<p><strong>Amazon has filed an intention to appeal the US Department of Defense&#39;s decision to give a major contract to Microsoft.</strong></p>\r\n\r\n<p>Amazon had been considered the favourite to win the deal, worth $10bn over the next 10 years.</p>\r\n\r\n<p>The company, which already provides cloud computing to the US Central Intelligence Agency, said the decision was made due to political pressure.</p>\r\n\r\n<p>In July, President Donald Trump threatened to intervene after what he described as &quot;tremendous complaints&quot;.</p>\r\n\r\n<p>Mr Trump had previously attacked Amazon chief executive Jeff Bezos, owner of the Washington Post, which has been critical of his presidency.</p>\r\n\r\n<p>The Pentagon subsequently delayed its decision to award the contract until 25 October, when it was announced the work would be given to Microsoft.</p>\r\n\r\n<ul>\r\n	<li><a href="https://www.bbc.com/news/technology-50191242">Pentagon snubs Amazon for $10bn &#39;Jedi&#39; contract</a></li>\r\n</ul>\r\n\r\n<p>Defence Secretary Mark Esper said the competition was fair.</p>\r\n\r\n<p>&quot;I am confident it was conducted freely and fairly without any type of outside influence,&quot; he told reporters in the South Korean capital Seoul.</p>\r\n\r\n<p>The Joint Enterprise Defense Infrastructure project - known as JEDI - is designed to modernise the antiquated data and communication systems within the US military. The contract is considered to be particularly lucrative if other government departments follow the Defense Department&#39;s lead when upgrading their own systems.</p>\r\n\r\n<p>An Amazon spokesperson told the BBC: &quot;Amazon Web Services is uniquely experienced and qualified to provide the critical technology the US military needs, and remains committed to supporting the DoD&#39;s modernisation efforts.</p>\r\n\r\n<p>&quot;We also believe it&#39;s critical for our country that the government and its elected leaders administer procurements objectively and in a manner that is free from political influence.</p>\r\n\r\n<p>&quot;Numerous aspects of the JEDI evaluation process contained clear deficiencies, errors and unmistakable bias - and it&#39;s important that these matters be examined and rectified.&quot;</p>\r\n\r\n<p>The BBC understands Amazon submitted its intention to protest against the decision to the Court of Federal Claims last Friday. The formal appeal itself will be filed at a later stage.</p>\r\n\r\n<p>Microsoft did not respond to requests for comment.</p>\r\n\r\n<p>Four companies had initially been in the running for the deal when the process was launched two years ago. IBM was eliminated, as was Oracle - which lodged an unsuccessful legal challenge alleging conflict of interest stemming from Amazon&#39;s hiring of two former Defense Department employees. Both were said to have been involved in the JEDI selection process.</p>\r\n', '293123751.jpg', 0, '2019-11-16 12:20:13', 0, 1, 1, 9);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`) VALUES
(1, 'user'),
(2, 'creator'),
(3, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `register_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role_id` int(11) DEFAULT NULL,
  `special_permissions` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `surname`, `email`, `username`, `password`, `register_date`, `role_id`, `special_permissions`) VALUES
(1, 'Dorian', 'Rinaa', 'dorian.rina@gmail.com', 'dori12348', '$2a$08$kSPpH3fA5yj4Iebe7coMneDIDdn9vC0o9/Gi84KLJteLLJBcKbzu.', '2019-10-20 15:06:31', 3, 0),
(2, 'Dorian', 'Rina', 'dorian@gmail.com', 'dori.rina', '$2a$08$y/A43eEJc6C3OmczXI0wLu8hbnf0/cYL01D6/KIBeH4hZIpQWgNCG', '2019-10-20 15:14:23', 3, 0),
(5, 'dori', 'rina', 'dorian.rina@atis.al', 'doririna', '14c8490365fef265e25fc663e46cd4bf1fe39404', '2019-10-21 12:59:39', 1, 0),
(6, 'Blog', 'blog', 'jdq81968@idxue.com', 'blog', 'd5c2c73a2241d107f9b4597ec26ee1de058d11b9', '2019-10-21 13:01:28', 1, 0),
(7, 'dor', 'fffffff', 'cnbbbbbbbbbbbb@gdhhd.com', 'dejjjjjjjjjjj', '$2y$13$rmSSMI75TIcfeMDh6KawsOr4qoAg2py1Udro3nl7ZNt5PHGNloCL2', '2019-10-23 13:34:31', 1, 0),
(8, 'ffffffff', 'ssssssssssss', 'dddddddd@sjsj.com', 'dori5', '$2y$13$4aKOp5DGe2GQq0r/VY8YIeMBDYa7ypCJtCWU9yHoRyTRAEU24AlNC', '2019-10-23 13:38:28', 1, 0),
(9, 'doriii', 'rinaa', 'dori@sdd.com', 'dori_rina', '$2a$08$aO.agjqQ449q53hEUexG1O34/wSiKpXyBfssIOH2d83N9zOmzHEB.', '2019-10-24 12:53:41', 3, 1),
(10, 'dori', 'nnjnnn', 'nnnd@dhdhd.com', 'nnnnnn', '$2a$08$rQczlnzVVmMhaWwOTPwciOGzxpXupkHXryiQ2VZCGYbMOTd3AJC32', '2019-10-30 11:24:44', 3, 0),
(11, 'doososjs', 'deeddf', 'djffjfjfj@gfhfdgf.com', 'dorian', '$2a$08$7/3I2SjT1fC05iB83i4vce9sSyAXvQAp0DsF9Bmy3Osr/U28yNoxW', '2019-11-01 13:38:15', 1, 0),
(15, 'dhhhhhhhhhh', 'fhhhhhh', 'ddddddddddddd@sgsg.co', 'dffggggggggggggggggg', '$2a$08$Kj5e1FajvrEKCrNU5sem.O9r7hakInI0XJq8HBGLkeC.5hcSi5N1C', '2019-11-11 16:03:07', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `role_id` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `posts_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
