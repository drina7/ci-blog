<h2>CODEIGNITER BLOG PROJECT WRITTEN BY DORIAN RINA</h2><br>

<h3>INSTALLATION </h3>
Setup a database and user. Go into PHPMYADMIN and import "ci-blog.sql" into your newly created database by going to import>choose "ci-blog.sql">choose import.<br> 
Configure the database file by going to /application/config/database.php <br>
Configure the base_url by going to /application/config/config.php <br>
Your site should now be installed.